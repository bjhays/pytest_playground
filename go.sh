#!/bin/bash
if [ ! -f "venv27/bin/python2.7" ]; then
    echo "No venv"
    virtualenv -p /usr/bin/python2.7 venv27 || exit 1
fi
. venv27/bin/activate || exit

extras="" # custom pip repo

pip install --upgrade pip  || exit 1
pip install $extras -r requirements.txt

py.test --driver Firefox

deactivate
